package com.example.playerservice.repository;

import com.example.playerservice.model.PlayerEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlayerRepository {

    public List<PlayerEntity> findAll();
    public List<PlayerEntity> findAllWithPagination(Integer pageNumber, Integer pageSize);
    public List<PlayerEntity> findById(String playerId);
}
