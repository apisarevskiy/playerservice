package com.example.playerservice.repository.impl;

import com.example.playerservice.model.PlayerEntity;
import com.example.playerservice.repository.PlayerRepository;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Repository
public class PlayerRepositoryImpl implements PlayerRepository {

    @Value("${data.pathFilePlayerCsv}")
    private String pathFilePlayerCsv;

    @Override
    public List<PlayerEntity> findAll() {

        List<PlayerEntity> players = new ArrayList<>();

        try (FileReader fileReader = new FileReader(pathFilePlayerCsv))
        {
            players = new CsvToBeanBuilder<PlayerEntity>(fileReader)
                                                 .withType(PlayerEntity.class)
                                                 .build()
                                                 .parse();
        } catch (IOException ex) {
            log.error("Error getting information about players from file csv ", ex);
        }

        return players;
    }

    @Override
    public List<PlayerEntity> findAllWithPagination(Integer pageNumber, Integer pageSize) {

        List<PlayerEntity> players = new ArrayList<>();
        int numberOfPages = 0;
        int startIndex = 0;
        int endIndex = 0;

        System.out.println(pathFilePlayerCsv);

        try (FileReader fileReader = new FileReader(pathFilePlayerCsv))
        {
            players = new CsvToBeanBuilder<PlayerEntity>(fileReader)
                                                .withType(PlayerEntity.class)
                                                .build()
                                                .parse();

            numberOfPages = players.size()/pageSize == 0 ? 1 : players.size()/pageSize + 1;
            startIndex = (pageNumber - 1) * pageSize == 0 ? 0 : (pageNumber - 1) * pageSize;
            endIndex = Math.min(startIndex + pageSize, players.size());

        } catch (IOException ex) {
            log.error("Error getting information about players from file csv ", ex);
        }

        return (pageNumber > numberOfPages) ? Collections.emptyList()
                                            : players.subList(startIndex, endIndex);
    }

    @Override
    public List<PlayerEntity> findById(String playerId) {

        List<PlayerEntity> players = new ArrayList<>();

        try (FileReader fileReader = new FileReader(pathFilePlayerCsv))
        {
            players = new CsvToBeanBuilder<PlayerEntity>(fileReader)
                                        .withType(PlayerEntity.class)
                                        .build()
                                        .parse()
                                        .stream().filter(s -> s.getPlayerID()
                                        .equals(playerId))
                                        .collect(Collectors.toList());;
        } catch (IOException ex) {
            log.error("Error getting information about player with ID {} from file csv ", playerId, ex);
        }

        return players;
    }


}
