package com.example.playerservice.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvDate;
import lombok.*;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

//@Getter
//@Setter
//@ToString
//@Builder
@Data
public class PlayerEntity {

//    playerID,birthYear,birthMonth,birthDay,birthCountry,birthState,birthCity,deathYear,deathMonth,deathDay,deathCountry,deathState,deathCity,nameFirst,nameLast,nameGiven,weight,height,bats,throws,debut,finalGame,retroID,bbrefID

    @CsvBindByName(column = "playerID", required = true)
    private String playerID;

    @CsvBindByName(column = "birthYear")
    private int birthYear;

    @CsvBindByName(column = "birthMonth")
    private int birthMonth;

    @CsvBindByName(column = "birthDay")
    private int birthDay;

    @CsvBindByName(column = "birthCountry")
    private String birthCountry;

    @CsvBindByName(column = "birthState")
    private String birthState;

    @CsvBindByName(column = "birthCity")
    private String birthCity;

    @CsvBindByName(column = "deathYear")
    private int deathYear;

    @CsvBindByName(column = "deathMonth")
    private int deathMonth;

    @CsvBindByName(column = "deathDay")
    private int deathDay;

    @CsvBindByName(column = "deathCountry")
    private String deathCountry;

    @CsvBindByName(column = "deathState")
    private String deathState;

    @CsvBindByName(column = "deathCity")
    private String deathCity;

    @CsvBindByName(column = "nameFirst")
    private String nameFirst;

    @CsvBindByName(column = "nameLast")
    private String nameLast;

    @CsvBindByName(column = "nameGiven")
    private String nameGiven;

    @CsvBindByName(column = "weight")
    private float weight;

    @CsvBindByName(column = "height")
    private float height;

    @CsvBindByName(column = "bats")
    private char bats;

    @CsvBindByName(column = "throws")
    private char throwss;

    @CsvBindByName(column = "debut")
    @CsvDate("yyyy-MM-dd")
    private LocalDate debut;

    @CsvBindByName(column = "finalGame")
    @CsvDate("yyyy-MM-dd")
    private LocalDate finalGame;

    @CsvBindByName(column = "retroID")
    private String retroID;

    @CsvBindByName(column = "bbrefID")
    private String bbrefID;
}
