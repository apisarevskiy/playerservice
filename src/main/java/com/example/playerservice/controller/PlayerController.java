package com.example.playerservice.controller;

import com.example.playerservice.model.PlayerEntity;
import com.example.playerservice.service.PlayerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Tag(name = "API for getting data about players", description = "To get data, make requests to swagger")
@RestController
@RequestMapping(value = "/api/players")
@RequiredArgsConstructor
public class PlayerController {

    private final PlayerService playerService;

    @Operation(summary = "Returns the list of all players. NOT RECOMMENDED to use with large data volumes. " +
                         "It is better to use an alternative option with pagination.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The list of players has been found"),
            @ApiResponse(responseCode = "404", description = "The list of players was not found", content = { @Content(schema = @Schema()) }),
            @ApiResponse(responseCode = "400", description = "Bad request", content = { @Content(schema = @Schema()) })
    })
    @GetMapping
    public ResponseEntity<List<PlayerEntity>> getPlayers() {

        List<PlayerEntity> players = playerService.getAllPlayers();

        return players == null ? new ResponseEntity<>(null, HttpStatus.NOT_FOUND)
                : new ResponseEntity<>(players, HttpStatus.OK);
    }

    @Operation(summary = "Returns the list of all players with pagination")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The list of players has been found"),
            @ApiResponse(responseCode = "404", description = "The list of players was not found", content = { @Content(schema = @Schema()) }),
            @ApiResponse(responseCode = "400", description = "Bad request", content = { @Content(schema = @Schema()) })
    })
    @GetMapping(value = "/{pageNumber}/{pageSize}")
    public ResponseEntity<List<PlayerEntity>> getPlayers(@PathVariable(value = "pageNumber") Integer pageNumber,
                                                         @PathVariable(value = "pageSize") Integer pageSize) {

        List<PlayerEntity> players = playerService.getAllPlayersWithPagination(pageNumber, pageSize);

        return players == null ? new ResponseEntity<>(null, HttpStatus.NOT_FOUND)
                               : new ResponseEntity<>(players, HttpStatus.OK);
    }

    @Operation(summary = "Returns a single player by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The player has been found"),
            @ApiResponse(responseCode = "404", description = "The player was not found", content = { @Content(schema = @Schema()) }),
            @ApiResponse(responseCode = "400", description = "Bad request", content = { @Content(schema = @Schema()) })
    })
    @GetMapping(value = "/{playerID}")
    public ResponseEntity<PlayerEntity> getAsset(@PathVariable(value = "playerID") String playerId) {

        PlayerEntity player = playerService.getPlayerById(playerId);

        return player == null ? new ResponseEntity<>(null, HttpStatus.NOT_FOUND)
                              : new ResponseEntity<>(player, HttpStatus.OK);
    }

}
