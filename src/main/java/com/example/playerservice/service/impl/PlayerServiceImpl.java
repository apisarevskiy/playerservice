package com.example.playerservice.service.impl;

import com.example.playerservice.model.PlayerEntity;
import com.example.playerservice.repository.PlayerRepository;
import com.example.playerservice.service.PlayerService;
import com.google.common.base.Stopwatch;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
@RequiredArgsConstructor
public class PlayerServiceImpl implements PlayerService {

    private final PlayerRepository playerRepository;
    @Override
    public List<PlayerEntity> getAllPlayers() {

        log.info("Started receiving a list of players ........");
        Stopwatch stopwatch = Stopwatch.createStarted();

        List<PlayerEntity> players = Collections.emptyList();

        try {
            players = playerRepository.findAll();
            log.info("Finished getting the list of players {} ms.", stopwatch.elapsed(TimeUnit.MILLISECONDS));

        } catch(Exception ex) {
            log.error("Error getting list of players {} ms.", stopwatch.elapsed(TimeUnit.MILLISECONDS), ex);
        }

        return players.size() == 0 ? null : players;

    }

    @Override
    public List<PlayerEntity> getAllPlayersWithPagination(Integer pageNumber, Integer pageSize) {

        log.info("Started receiving a list of players with pagination........");
        Stopwatch stopwatch = Stopwatch.createStarted();

        List<PlayerEntity> players = Collections.emptyList();

        try {
            players = playerRepository.findAllWithPagination(pageNumber, pageSize);
            log.info("Finished getting the list of players {} ms.", stopwatch.elapsed(TimeUnit.MILLISECONDS));

        } catch(Exception ex) {
            log.error("Error getting list of players {} ms.", stopwatch.elapsed(TimeUnit.MILLISECONDS), ex);
        }

        return players.size() == 0 ? null : players;
    }

    @Override
    public PlayerEntity getPlayerById(String playerId) {

        log.info("[{}]. Started receiving information about player by his ID ........", playerId);
        Stopwatch stopwatch = Stopwatch.createStarted();

        List<PlayerEntity> players = Collections.emptyList();

        try {
            players = playerRepository.findById(playerId);
            log.info("[{}]. Finished getting information about player by his ID {} ms.", playerId,
                                                                 stopwatch.elapsed(TimeUnit.MILLISECONDS));

        } catch(Exception ex) {
            log.error("[{}]. getting information about player by his ID {} ms.", playerId,
                                                                 stopwatch.elapsed(TimeUnit.MILLISECONDS), ex);
        }

        return players.size() == 0 ? null
                                   : players.get(0);
    }
}
