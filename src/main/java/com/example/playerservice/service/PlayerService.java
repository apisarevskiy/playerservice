package com.example.playerservice.service;

import com.example.playerservice.model.PlayerEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PlayerService {

    public List<PlayerEntity> getAllPlayers();
    public List<PlayerEntity> getAllPlayersWithPagination(Integer pageNumber, Integer pageSize);
    public PlayerEntity getPlayerById(String playerId);

}
