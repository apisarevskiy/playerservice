# Getting Started

### Reference Documentation

1. Start the service.
   You can run service: java -jar target/PlayerService.jar
   To check the service use the swagger API link below to check the functionality of the service:

   [http://localhost:8087/swagger-ui/index.html]()


2. Or you can create a docker image and run the container using the following commands:

     ### command to create image below

        docker build . -t intuit/player-service:latest

     ### command to create conteiner below

        docker-compose up -d 

   After that you can use the swagger API link below to check the functionality of the service:

   [http://localhost:8087/swagger-ui/index.html]()

     ### command to destroy conteiner below

        docker-compose down