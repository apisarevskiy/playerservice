FROM eclipse-temurin:17-jdk-alpine
VOLUME /tmp
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
COPY src/main/resources/*.csv /src/main/resources/
EXPOSE 8087
ENTRYPOINT ["java", "-jar","/app.jar"]